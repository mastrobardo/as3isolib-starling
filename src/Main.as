package 
{
	import examples.Example01;
	import examples.Example02;
	import examples.Example03;
	import examples.Example04;
	import examples.Example05;
	import examples.Example06;
	import examples.Example07;
	import examples.Example08;
	import examples.Example09;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import starling.core.Starling;
	
	/**
	 * ...
	 * @author pautay
	 */
	public class Main extends Sprite 
	{
		public static var STAGE : Stage;
		
		private var _viewport:Rectangle;
		private var _starling : Starling;
		
		public function Main()
		{	
			STAGE = stage;
			
			_viewport = new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight);
			
			//_starling = new Starling(Example01, stage, _viewport);
			//_starling = new Starling(Example02, stage, _viewport);
			//_starling = new Starling(Example03, stage, _viewport);
			//_starling = new Starling(Example04, stage, _viewport);
			//_starling = new Starling(Example05, stage, _viewport);
			//_starling = new Starling(Example06, stage, _viewport);
			//_starling = new Starling(Example07, stage, _viewport);			
			//_starling = new Starling(Example08, stage, _viewport);			
			_starling = new Starling(Example09, stage, _viewport);			
			_starling.simulateMultitouch  = false;
            _starling.enableErrorChecking = false;
			_starling.showStats = true ;
			
			_starling.stage3D.addEventListener(Event.CONTEXT3D_CREATE, _start);
			
			trace("width : " + Starling.current.viewPort.width + ", height : " + Starling.current.viewPort.height);
		}
		
		private function _start(e:Event):void 
		{
			 _starling.start();			 
		}
	}	
}